## oh-my-posh init pwsh --config 'C:\pshell\pthemes\theme1.json' | Invoke-Expression ##
Import-Module -Name Terminal-Icons
Set-Alias lvim 'C:\Users\chris\.local\bin\lvim.ps1'
Invoke-Expression (&starship init powershell)
C:\Users\chris\OneDrive\Desktop\PShellScripts\dragon.ps1
# LIST OF FUNCTIONS BEGINS HERE
function lw {
    Get-ChildItem | Format-Wide -Column 4
}
function touch {
    [CmdletBinding()]
    param (
        [Parameter()]
        [String]$Name
    )
    New-Item -Name $Name -ItemType File
}
function mdir {
    [CmdletBinding()]
    param (
        [Parameter()]
        [String]$Name
    )
    New-Item -Name $Name -ItemType Directory
}
