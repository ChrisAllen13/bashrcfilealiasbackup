# Created by newuser for 5.9
export PATH="/home/$user/.local/bin:$PATH"
   
# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.
# History in cache directory:
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history

# Basic auto/tab complete:
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.


# vi mode
bindkey -v
export KEYTIMEOUT=1


##################
# Custom Commands #
#################
alias vi='nvim'
alias Ss='pacman -Ss'
alias ySs='yay -Ss'
alias install='sudo pacman -S'
alias yin='yay -S'
alias up='sudo pacman -Syu'
alias yup='yay -Syu'
alias sd='sudo systemctl poweroff'
alias sr='sudo systemctl reboot'
alias brc='vi ~/.bashrc'
alias bs='source ~/.bashrc'
alias ls='exa -al --color=always --icons  --group-directories-first'
alias la='exa -a --color=always --icons  --group-directories-first'
alias ll='exa -l --color=always --icons  --group-directories-first'
alias ..='cd ..'
alias rem='sudo pacman -R'
alias yrem='yay -R'
alias allp='pacman -Qe'
alias yallp='yay -Qe'
alias qt='lvim ~/.config/qtile/config.py'
alias gcm='git commit -m'
alias gaa='git add --all'
alias ga='git add'
alias gp='git push'
alias gl='git log'
alias gc='git clone'
alias locate='sudo find / -name'
alias flocate='sudo find / -wholename'
alias star='lvim ~/.config/starship.toml'
alias cat='bat'
alias storage='du -sh * | sort -h'
alias zrc='lvim ~/.zshrc'
alias zs='source ~/.zshrc'
alias calc='speedcrunch'
alias fss='flameShotScreenPrompter'
alias fsg='flameShotGUIPrompter'
alias neofetch='neofetch --ascii_distro OPENBSD'

eval "$(starship init zsh)"

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
bindkey -v '^?' backward-delete-char

# load zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null

# Custom keyboard settings

# Custom manpager set to bat
export MANPAGER="sh -c 'col -bx | bat -l man -p'"


##################
# Custom Commands #
#################
function flameShotGUIPrompter()
{ echo -n "insert the name of your image here: "
	read -r imageName
	echo "Saved to ~/Screenshots/$imageName"
	flameshot gui --path ~/Screenshots/$imageName 
}
function flameShotScreenPrompter()
{ echo -n "insert the name of your image here: "
	read -r imageName
	echo "Saved to ~/Screenshots/$imageName"
	flameshot screen --path ~/Screenshots/$imageName 
}
